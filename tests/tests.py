from datetime import datetime
from dateutil import parser
import hashlib
import os
from textwrap import dedent
import unittest

from feedinator.views import generate_entry, generate_feed, parse_page

this_dir = os.path.dirname(__file__)

# TODO
# * Deal with conversion of datatypes


class ParsePageTest(unittest.TestCase):

    maxDiff = None

    def test_parse_page_swingplanit(self):
        definition = {
            'css': 'ul.homepagelist li',
            'columns': [
                {'css': 'a', 'attr': 'href', 'name': 'url'},
                {'css': 'div.daycalendar', 'name': 'day'},
                {'css': 'div.maintitle2', 'name': 'title'},
                {'css': 'div.pins', 'name': 'location'},
                {'css': 'div.circcomp', 'name': 'competition'},
                {'css': "div.circconf", 'name': 'teachers'},
                {'css': "div.circexc", 'name': 'exchange'},
                {'previous': 'h3', 'name': 'month'},
            ],
            'transforms': {
                'date': lambda x: parser.parse(x['day'] + ' ' + x['month'])
            }
        }

        with open(os.path.join(this_dir, 'files/swingplanit.html'), 'r') as f:
            html = f.read()
        print parse_page(definition, html)[0]
        self.assertEqual(
            {
                'count': 0,
                'url': 'http://www.swingplanit.com/event/midwest-lindyfest1',
                'month': u'May 2014',
                'day': u'8th',
                'title': u'Midwest Lindyfest',
                'location': u'United States',
                'competition': u'Competitions Held',
                'date': datetime(2014, 5, 8)
            },
            parse_page(definition, html)[0]
        )
        self.assertEqual(
            {
                'count': 1,
                'url': 'http://www.swingplanit.com/event/northern-boogie',
                'month': u'May 2014',
                'day': u'8th',
                'title': u'Northern Boogie',
                'location': u'Russian Federation',
                'competition': u'Competitions Held',
                'teachers': u'Teachers Confirmed',
                'date': datetime(2014, 5, 8)
            },
            parse_page(definition, html)[1]
        )
        self.assertEqual(
            {
                'count': 10,
                'url': 'http://www.swingplanit.com/event/edinburgh-balboa-exchange',
                'month': u'May 2014',
                'day': u'16th',
                'title': u'Edinburgh Balboa Exchange',
                'location': u'United Kingdom',
                'exchange': u'Exchange',
                'date': datetime(2014, 5, 16)
            },
            parse_page(definition, html)[10]
        )


    def test_parse_page_swingoutlondon_classes(self):
        definition = {
            'css': 'div#classes ul.daylist ul.unstyled li',
            'columns': [
                {'css': 'a.postcode', 'attr': 'href', 'name': 'maplink'},
                {'css': 'a.postcode', 'name': 'postcode'},
                {'css': 'span.details', 'name': 'title'},
                {'parent': '*', 'css': 'h3', 'name': 'day'},
                {'css': 'span.details a', 'attr': 'href', 'name': 'url'},
            ]
        }

        with open(os.path.join(this_dir, 'files/swingoutlondon.html'), 'r') as f:
            html = f.read()
        self.assertEqual(
            {
                'count': 0,
                'title': 'New! Bethnal Green with Swing Patrol',
                'url': 'http://www.swingpatrol.co.uk/venues/bethnal-green/',
                'day': 'Monday',
                'maplink': 'map/classes/Monday/380',
                'postcode': 'E2',
            },
            parse_page(definition, html)[0]
        )
        self.assertEqual(
            {
                'count': 14,
                'title': 'Acton with Hot-Cha Swing',
                'url': 'http://www.hotchaswing.com/classes.html#acton',
                'day': 'Tuesday',
                'maplink': 'map/classes/Tuesday/372',
                'postcode': 'W3',
            },
            parse_page(definition, html)[14]
        )


    def test_parse_page_swingoutlondon_socials(self):
        definition = {
            'css': 'div#socials ul.datelist ul.unstyled li',
            'columns': [
                {'css': 'a.postcode', 'attr': 'href', 'name': 'maplink'},
                {'css': 'a.postcode', 'name': 'postcode'},
                {'css': 'span.details', 'name': 'title'},
                {'parent': '*', 'css': 'h3', 'name': 'day'},
                {'css': 'span.details a', 'attr': 'href', 'name': 'url'},
            ],
            'transforms': {
                'date': lambda x: parser.parse(x['day'].replace('Today', '').replace('Tomorrow', ''))
            }
        }

        with open(os.path.join(this_dir, 'files/swingoutlondon.html'), 'r') as f:
            html = f.read()
        self.assertEqual(
            {
                'count': 0,
                'title': 'Boogaloo Bounce (afternoon-early evening) - The Boogaloo in Highgate',
                'url': 'http://www.swingpatrol.co.uk/social-dancing/boogaloo-bounce/',
                'day': 'Today Saturday 17th May',
                'maplink': 'map/socials/2014-05-17/335',
                'postcode': 'N6',
                'date': datetime(2014, 5, 17)
            },
            parse_page(definition, html)[0]
        )
        self.assertEqual(
            {
                'count': 3,
                'title': "Ewan Bleach's Sugar Foot Sessions - The Russet in Hackney Downs",
                'url': 'http://www.therusset.co.uk/events',
                'day': 'Tomorrow Sunday 18th May',
                'maplink': 'map/socials/2014-05-18/377',
                'postcode': 'E8',
                'date': datetime(2014, 5, 18)
            },
            parse_page(definition, html)[3]
        )



class GenerateEntryTest(unittest.TestCase):

    def test_generate_entry_competitions(self):
        entry_description = {
            'count': 0,
            'url': 'http://www.swingplanit.com/event/midwest-lindyfest1',
            'month': u'May 2014',
            'day': u'8th',
            'title': u'Midwest Lindyfest',
            'location': u'United States',
            'competition': u'Competitions Held',
        }

        template = {
            "title": "${day} ${month} - ${title}",
            "content": dedent("""
                ${location}<br/>
                % if competition is not UNDEFINED:
                    ${competition}<br/>
                % endif
                % if teachers is not UNDEFINED:
                    ${teachers}<br/>
                % endif
                % if exchange is not UNDEFINED:
                    ${exchange}
                % endif
            """)
        }

        entry = generate_entry(entry_description, template)

        self.assertEqual(entry.title, "8th May 2014 - Midwest Lindyfest")
        self.assertEqual(entry.id, 'http://feed.millenniumhand.co.uk/' + hashlib.md5('http://www.swingplanit.com/event/midwest-lindyfest1').hexdigest())
        self.assertIn("United States", entry.content)
        self.assertIn("Competitions Held", entry.content)
        self.assertEqual(entry.url, entry_description["url"])


    def test_generate_entry_teachers(self):
        entry_description = {
            'count': 0,
            'url': 'http://www.swingplanit.com/event/midwest-lindyfest1',
            'month': u'May 2014',
            'day': u'8th',
            'title': u'Midwest Lindyfest',
            'location': u'United States',
            'teachers': u'Teachers Confirmed',
        }

        template = {
            "title": "${day} ${month} - ${title}",
            "content": dedent("""
                ${location}<br/>
                % if competition is not UNDEFINED:
                    ${competition}<br/>
                % endif
                % if teachers is not UNDEFINED:
                    ${teachers}<br/>
                % endif
                % if exchange is not UNDEFINED:
                    ${exchange}
                % endif
            """)
        }

        entry = generate_entry(entry_description, template)

        self.assertEqual(entry.title, "8th May 2014 - Midwest Lindyfest")
        self.assertEqual(entry.id, 'http://feed.millenniumhand.co.uk/' + hashlib.md5('http://www.swingplanit.com/event/midwest-lindyfest1').hexdigest())
        self.assertIn("United States", entry.content)
        self.assertIn("Teachers Confirmed", entry.content)
        self.assertEqual(entry.url, entry_description["url"])


    def test_generate_entry_exchange(self):
        entry_description = {
            'count': 0,
            'url': 'http://www.swingplanit.com/event/midwest-lindyfest1',
            'month': u'May 2014',
            'day': u'8th',
            'title': u'Midwest Lindyfest',
            'location': u'United States',
            'exchange': u'Exchange',
        }
        template = {
            "title": "${day} ${month} - ${title}",
            "content": dedent("""
                ${location}<br/>
                % if competition is not UNDEFINED:
                    ${competition}<br/>
                % endif
                % if teachers is not UNDEFINED:
                    ${teachers}<br/>
                % endif
                % if exchange is not UNDEFINED:
                    ${exchange}
                % endif
            """)
        }

        entry = generate_entry(entry_description, template)

        self.assertEqual(entry.title, "8th May 2014 - Midwest Lindyfest")
        self.assertEqual(entry.id, 'http://feed.millenniumhand.co.uk/' + hashlib.md5('http://www.swingplanit.com/event/midwest-lindyfest1').hexdigest())
        self.assertIn("United States", entry.content)
        self.assertIn("Exchange", entry.content)
        self.assertEqual(entry.url, entry_description["url"])
        self.assertIn("<br/>", entry.content)


class GenerateFeedTest(unittest.TestCase):

    def test_generate_feed(self):
        entries = [
            {
                'count': 0,
                'url': 'http://www.swingplanit.com/event/midwest-lindyfest1',
                'month': u'May 2014',
                'day': u'8th',
                'title': u'Midwest Lindyfest',
                'location': u'United States',
                'competition': u'Competitions Held',
            },
            {
                'count': 1,
                'url': 'http://www.swingplanit.com/event/northern-boogie',
                'month': u'May 2014',
                'day': u'8th',
                'title': u'Northern Boogie',
                'location': u'Russian Federation',
                'competition': u'Competitions Held',
                'teachers': u'Teachers Confirmed',
            },
            {
                'count': 2,
                'url': 'http://www.swingplanit.com/event/edinburgh-balboa-exchange',
                'month': u'May 2014',
                'day': u'16th',
                'title': u'Edinburgh Balboa Exchange',
                'location': u'United Kingdom',
                'exchange': u'Exchange',
            }
        ]
        template = {
            "feed_title": "Swing Planit",
            "url": "http://swingplanit.com",
            "feed_url": "http://feeds.millenniumhand.co.uk/swingplanit",
            "title": "${day} ${month} - ${title}",
            "content": dedent("""
                ${location}<br/>
                % if competition is not UNDEFINED:
                    ${competition}<br/>
                % endif
                % if teachers is not UNDEFINED:
                    ${teachers}<br/>
                % endif
                % if exchange is not UNDEFINED:
                    ${exchange}
                % endif
            """)
        }

        feed = generate_feed(entries, template)

        self.assertEqual(feed.title, "Swing Planit")
        self.assertEqual(feed.feed_url, "http://feeds.millenniumhand.co.uk/swingplanit")
        self.assertEqual(feed.url, "http://swingplanit.com")
        self.assertEqual(len(feed.entries), 3)
