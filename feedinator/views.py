from feedinator import app, feeds

from bs4 import BeautifulSoup
from datetime import datetime
from flask import render_template_string, request
import hashlib
from mako.template import Template
import requests
from urlparse import urlparse
from werkzeug.contrib.atom import AtomFeed, FeedEntry


def parse_page(definition, html):
    soup = BeautifulSoup(html)
    results = []
    count = 0
    for entry in soup.select(definition['css']):
        e = {'count': count}
        for column in definition['columns']:
            element = entry
            if 'previous' in column:
                element = element.find_previous(column['previous'])
            if 'parent' in column:
                element = element.find_parents('li')[-1]
            if 'css' in column:
                try:
                    element = element.select(column['css'])[0]
                except IndexError:
                    continue
            if 'attr' in column:
                value = element[column['attr']]
            else:
                value = "".join(element.strings)
            e[column['name']] = value

        if 'transforms' in definition:
            for name, transform in definition['transforms'].items():
                e[name] = transform(e)
        results.append(e)
        count += 1
    return results


def generate_feed(entries, template):
    feed = AtomFeed(
        title=template["feed_title"],
        url=template["url"],
        feed_url=template["feed_url"],
    )
    for entry in entries:
        rendered_entry = generate_entry(entry, template)
        if rendered_entry:
            feed.add(rendered_entry)
    return feed


def generate_entry(entry_description, template):
    if "url" not in entry_description:
        return None
    return FeedEntry(
        title=Template(template["title"]).render(**entry_description),
        content=Template(template["content"]).render(**entry_description),
        url=entry_description["url"],
        id="http://feed.millenniumhand.co.uk/" + hashlib.md5(entry_description["url"]).hexdigest(),
        updated=datetime.now()
    )


@app.route('/')
def index():
    base_url = urlparse(request.base_url)
    template = """
    <html>
    <body>
    <ul id="feeds">
    {% for item in feeds %}
    <li><a href="http://{{ base_url.netloc }}/feed/{{ item }}">{{ item }}</a></li>
    {% endfor %}
    </ul>
    </body>
    </html>
    """
    return render_template_string(template, feeds=feeds.feeds.keys(), base_url=base_url)



@app.route('/feed/<name>')
def feed(name):
    feed_definition = feeds.feeds[name]
    page = requests.get(feed_definition['url']).content
    entries = parse_page(feed_definition['extractor'], page)
    return generate_feed(entries, feed_definition)
