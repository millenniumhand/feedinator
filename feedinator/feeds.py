from dateutil import parser
from textwrap import dedent

swingplanit = {
    "extractor": {
        'css': 'ul.homepagelist li',
        'columns': [
            {'css': 'a', 'attr': 'href', 'name': 'url'},
            {'css': 'div.daycalendar', 'name': 'day'},
            {'css': 'div.maintitle2', 'name': 'title'},
            {'css': 'div.pins', 'name': 'location'},
            {'css': 'div.circcomp', 'name': 'competition'},
            {'css': "div.circconf", 'name': 'teachers'},
            {'css': "div.circexc", 'name': 'exchange'},
            {'previous': 'h3', 'name': 'month'},
        ]
    },
    'transforms': {
        'date': lambda x: parser.parse(x['day'] + ' ' + x['month'])
    },
    "title": "${day} ${month} - ${title}",
    "content": dedent("""
        ${location}<br/>
        % if competition is not UNDEFINED:
            ${competition}<br/>
        % endif
        % if teachers is not UNDEFINED:
            ${teachers}<br/>
        % endif
        % if exchange is not UNDEFINED:
            ${exchange}
        % endif
    """)
}

swingoutlondon_templates = {
    "title": "${day} - ${title}",
    "content": dedent("""
        % if day is not UNDEFINED:
        ${day} -
        % endif
        % if title is not UNDEFINED:
        ${title}<br/>
        % endif
        % if postcode is not UNDEFINED:
        ${postcode}<br/>
        % endif
        % if maplink is not UNDEFINED:
        <a href="http://www.swingoutlondon.co.uk/${maplink}">map link</a>
        % endif
    """)
}


def merge_dicts(d1, d2):
    return dict(d1, **d2)

feeds = {
    "swingplanit-blues-europe": merge_dicts(
        {
            "feed_title": "Swing Planit - Blues Europe",
            "url": "http://www.swingplanit.com/C10&C2",
            "feed_url": "http://feed.millenniumhand.co.uk/feed/swingplanit-blues-europe",
        },
        swingplanit
    ),
    "swingplanit-balboa-europe": merge_dicts(
        {
            "feed_title": "Swing Planit - Balboa Europe",
            "url": "http://www.swingplanit.com/C10&C7",
            "feed_url": "http://feed.millenniumhand.co.uk/feed/swingplanit-balboa-europe",
        },
        swingplanit
    ),
    "swingplanit-lindy-europe": merge_dicts(
        {
            "feed_title": "Swing Planit - Lindy Hop Europe",
            "url": "http://www.swingplanit.com/C10&C1",
            "feed_url": "http://feed.millenniumhand.co.uk/feed/swingplanit-lindy-europe",
        },
        swingplanit
    ),
    "swingoutlondon-classes": merge_dicts(
        {
            "feed_title": "Swingout London - Classes",
            "url": "http://www.swingoutlondon.co.uk/",
            "feed_url": "http://feed.millenniumhand.co.uk/feed/swingoutlondon-classes",
            "extractor": {
                'css': 'div#classes ul.daylist ul.unstyled li',
                'columns': [
                    {'css': 'a.postcode', 'attr': 'href', 'name': 'maplink'},
                    {'css': 'a.postcode', 'name': 'postcode'},
                    {'css': 'span.details', 'name': 'title'},
                    {'parent': '*', 'css': 'h3', 'name': 'day'},
                    {'css': 'span.details a', 'attr': 'href', 'name': 'url'},
                ]
            },
        },
        swingoutlondon_templates
    ),
    "swingoutlondon-socials": merge_dicts(
        {
            "feed_title": "Swingout London - Socials",
            "url": "http://www.swingoutlondon.co.uk/",
            "feed_url": "http://feed.millenniumhand.co.uk/feed/swingoutlondon-socials",
            "extractor": {
                'css': 'div#socials ul.datelist ul.unstyled li',
                'columns': [
                    {'css': 'a.postcode', 'attr': 'href', 'name': 'maplink'},
                    {'css': 'a.postcode', 'name': 'postcode'},
                    {'css': 'span.details', 'name': 'title'},
                    {'parent': '*', 'css': 'h3', 'name': 'day'},
                    {'css': 'span.details a', 'attr': 'href', 'name': 'url'},
                ]
            },
            'transforms': {
                'date': lambda x: parser.parse(x['day'].replace('Today', '').replace('Tomorrow', ''))
            }
        },
        swingoutlondon_templates
    )
}
